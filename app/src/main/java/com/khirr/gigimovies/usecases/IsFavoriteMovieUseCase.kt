package com.khirr.gigimovies.usecases

import android.util.Log
import com.khirr.gigimovies.data.models.Movie
import com.khirr.gigimovies.data.parsers.toFavoriteMovieEntity
import com.khirr.gigimovies.data.room.LocalMovieDatabaseManager
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.core.component.KoinComponent

class IsFavoriteMovieUseCase(
    private val localMovieDatabaseManager: LocalMovieDatabaseManager
): KoinComponent {
    fun invoke(movie: Movie): Single<Boolean> = localMovieDatabaseManager
        .database()
        .favoriteMovieDao()
        .count(movie.id)
        .map { it > 0 }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}