package com.khirr.gigimovies.usecases

import com.khirr.gigimovies.data.models.Movie
import com.khirr.gigimovies.data.parsers.toModel
import com.khirr.gigimovies.data.retrofit.RetrofitClient
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.core.component.KoinComponent

class FindPopularMoviesUseCase(
    private val retrofitClient: RetrofitClient
): KoinComponent {
    fun invoke(page: Int): Single<List<Movie>> = retrofitClient.retrofitMovieService()
        .findPopularMovies(page)
        .map { moviesServerResponse -> moviesServerResponse.results.map { it.toModel() } }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}