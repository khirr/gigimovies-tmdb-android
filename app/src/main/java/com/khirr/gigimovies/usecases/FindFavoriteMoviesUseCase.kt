package com.khirr.gigimovies.usecases

import com.khirr.gigimovies.data.models.Movie
import com.khirr.gigimovies.data.parsers.toFavoriteMovieEntity
import com.khirr.gigimovies.data.parsers.toModel
import com.khirr.gigimovies.data.room.LocalMovieDatabaseManager
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.core.component.KoinComponent

class FindFavoriteMoviesUseCase(
    private val localMovieDatabaseManager: LocalMovieDatabaseManager
): KoinComponent {
    fun invoke(): Flowable<List<Movie>> = localMovieDatabaseManager
        .database()
        .favoriteMovieDao()
        .findAll()
        .map { items -> items.map { it.toModel() } }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}