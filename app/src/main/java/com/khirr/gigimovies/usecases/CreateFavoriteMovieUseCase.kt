package com.khirr.gigimovies.usecases

import com.khirr.gigimovies.data.models.Movie
import com.khirr.gigimovies.data.parsers.toFavoriteMovieEntity
import com.khirr.gigimovies.data.parsers.toModel
import com.khirr.gigimovies.data.retrofit.RetrofitClient
import com.khirr.gigimovies.data.room.LocalMovieDatabaseManager
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.core.component.KoinComponent

class CreateFavoriteMovieUseCase(
    private val localMovieDatabaseManager: LocalMovieDatabaseManager
): KoinComponent {
    fun invoke(movie: Movie): Completable = localMovieDatabaseManager
        .database()
        .favoriteMovieDao()
        .insert(movie.toFavoriteMovieEntity())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}