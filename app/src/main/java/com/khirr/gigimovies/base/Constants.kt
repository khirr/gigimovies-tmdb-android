package com.khirr.gigimovies.base

object Constants {
    // API
    const val MOVIES_API_BASE_URL = "https://api.themoviedb.org/3/"
    const val MOVIES_API_KEY = "850017d738dcf67b279d2b68a0ddbf2e"
    const val MOVIES_API_DEFAULT_ITEMS_PER_PAGE = 20
    const val MOVIES_IMAGES_ORIGINAL_BASE_URL = "https://image.tmdb.org/t/p/original"
    const val MOVIES_IMAGES_THUMBNAIL_BASE_URL = "https://image.tmdb.org/t/p/w500"

    // Room
    const val MOVIES_ROOM_DATABASE_NAME = "gigimoviesdb"
}