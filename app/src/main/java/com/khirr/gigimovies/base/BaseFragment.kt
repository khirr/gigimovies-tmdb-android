package com.khirr.gigimovies.base

import androidx.fragment.app.Fragment

open class BaseFragment : Fragment {
    lateinit var title: String

    constructor(stringTitle: String) : super() {
        title = stringTitle
    }

    constructor(resTitle: Int) : super() {
        title = App.instance.getString(resTitle) ?: ""
    }
}