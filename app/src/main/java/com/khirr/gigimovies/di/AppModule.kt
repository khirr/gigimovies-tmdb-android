package com.khirr.gigimovies.di

import com.khirr.gigimovies.data.retrofit.RetrofitClient
import com.khirr.gigimovies.data.room.LocalMovieDatabaseManager
import com.khirr.gigimovies.presentation.FavoriteMoviesListViewModel
import com.khirr.gigimovies.presentation.MovieDetailViewModel
import com.khirr.gigimovies.presentation.PopularMoviesListViewModel
import com.khirr.gigimovies.presentation.SearchMoviesListViewModel
import com.khirr.gigimovies.usecases.*
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    // Retrofit
    single { RetrofitClient() }

    // Room
    single { LocalMovieDatabaseManager(androidApplication()) }

    // Use Cases
    factory { CreateFavoriteMovieUseCase(get()) }
    factory { DeleteFavoriteMovieUseCase(get()) }
    factory { IsFavoriteMovieUseCase(get()) }
    factory { FindFavoriteMoviesUseCase(get()) }
    factory { FindPopularMoviesUseCase(get()) }
    factory { SearchMoviesUseCase(get()) }

    // View Models
    viewModel { PopularMoviesListViewModel(get()) }
    viewModel { params -> MovieDetailViewModel(get(), get(), get(), params.get()) }
    viewModel { FavoriteMoviesListViewModel(get()) }
    viewModel { SearchMoviesListViewModel(get()) }
}