package com.khirr.gigimovies.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import com.khirr.gigimovies.base.BaseViewModel
import com.khirr.gigimovies.base.Event
import com.khirr.gigimovies.data.models.Movie
import com.khirr.gigimovies.usecases.FindFavoriteMoviesUseCase
import com.khirr.gigimovies.usecases.FindPopularMoviesUseCase

class FavoriteMoviesListViewModel(
    private val findFavoriteMovies: FindFavoriteMoviesUseCase
): BaseViewModel() {
    // Events to emit
    private val localEvents = MutableLiveData<Event<FavoriteMoviesListNavigation>>()
    val events: LiveData<Event<FavoriteMoviesListNavigation>> get() = localEvents

    val favoriteMoviesList: LiveData<List<Movie>>
        get() = LiveDataReactiveStreams.fromPublisher(findFavoriteMovies.invoke())

    sealed class FavoriteMoviesListNavigation {
        data class ShowFavoriteMovies(val movies: List<Movie>) : FavoriteMoviesListNavigation()
        object FavoriteMoviesEmpty : FavoriteMoviesListNavigation()
    }

    fun onFavoriteMoviesList(favoriteMovies: List<Movie>) {
        if (favoriteMovies.isEmpty()) {
            localEvents.value = Event(FavoriteMoviesListNavigation.ShowFavoriteMovies(arrayListOf()))
            localEvents.value = Event(FavoriteMoviesListNavigation.FavoriteMoviesEmpty)
        } else {
            localEvents.value = Event(FavoriteMoviesListNavigation.ShowFavoriteMovies(favoriteMovies))
        }
    }
}