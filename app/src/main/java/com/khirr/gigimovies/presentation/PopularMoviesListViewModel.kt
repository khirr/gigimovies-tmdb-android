package com.khirr.gigimovies.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.khirr.gigimovies.base.BaseViewModel
import com.khirr.gigimovies.base.Constants
import com.khirr.gigimovies.base.Event
import com.khirr.gigimovies.data.models.Movie
import com.khirr.gigimovies.usecases.FindPopularMoviesUseCase

class PopularMoviesListViewModel(
    private val findPopularMoviesUseCase: FindPopularMoviesUseCase
): BaseViewModel() {
    // Events to emit
    private val localEvents = MutableLiveData<Event<PopularMoviesListNavigation>>()
    val events: LiveData<Event<PopularMoviesListNavigation>> get() = localEvents

    // Lifecycle vars
    private var currentPage = 1
    private var isLoading = false
    private var isLastPage = false

    sealed class PopularMoviesListNavigation {
        data class ShowPopularMovies(val movies: List<Movie>): PopularMoviesListNavigation()
        data class PopularMoviesError(val error: Throwable): PopularMoviesListNavigation()
        object ShowLoading: PopularMoviesListNavigation()
        object HideLoading: PopularMoviesListNavigation()
    }

    fun onFindAllPopularMovies() {
        addDisposable {
            findPopularMoviesUseCase
                .invoke(currentPage)
                .doOnSubscribe { showIsLoading() }
                .subscribe({ popularMovies ->
                    if (popularMovies.size < Constants.MOVIES_API_DEFAULT_ITEMS_PER_PAGE) {
                        isLastPage = true
                    }

                    hideIsLoading()
                    localEvents.value =
                        Event(PopularMoviesListNavigation.ShowPopularMovies(popularMovies))
                }, { error ->
                    isLastPage = true
                    hideIsLoading()
                    localEvents.value = Event(PopularMoviesListNavigation.PopularMoviesError(error))
                })
        }
    }

    fun onLoadMorePopularMovies(visibleItemCount: Int, firstVisibleItemPosition: Int, totalItemCount: Int) {
        if (isLoading || isLastPage || !isInFooter(visibleItemCount, firstVisibleItemPosition, totalItemCount)) {
            return
        }

        currentPage += 1
        onFindAllPopularMovies()
    }

    private fun isInFooter(
        visibleItemCount: Int,
        firstVisibleItemPosition: Int,
        totalItemCount: Int
    ): Boolean {
        return visibleItemCount + firstVisibleItemPosition >= totalItemCount
                && firstVisibleItemPosition >= 0
                && totalItemCount >= Constants.MOVIES_API_DEFAULT_ITEMS_PER_PAGE
    }

    private fun showIsLoading() {
        isLoading = true
        localEvents.value = Event(PopularMoviesListNavigation.ShowLoading)
    }

    private fun hideIsLoading() {
        isLoading = false
        localEvents.value = Event(PopularMoviesListNavigation.HideLoading)
    }
}