package com.khirr.gigimovies.presentation

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.khirr.gigimovies.base.BaseViewModel
import com.khirr.gigimovies.base.Event
import com.khirr.gigimovies.data.models.Movie
import com.khirr.gigimovies.usecases.CreateFavoriteMovieUseCase
import com.khirr.gigimovies.usecases.DeleteFavoriteMovieUseCase
import com.khirr.gigimovies.usecases.IsFavoriteMovieUseCase

class MovieDetailViewModel(
    private val isFavoriteMovieUseCase: IsFavoriteMovieUseCase,
    private val createFavoriteMovieUserCase: CreateFavoriteMovieUseCase,
    private val deleteFavoriteMovieUseCase: DeleteFavoriteMovieUseCase,
    private val movie: Movie,
): BaseViewModel() {
    // Events to emit
    private val localEvents = MutableLiveData<Event<MovieDetailNavigation>>()
    val events: LiveData<Event<MovieDetailNavigation>> get() = localEvents
    private var isFavorite = false

    sealed class MovieDetailNavigation {
        object FavoriteCreated : MovieDetailNavigation()
        object FavoriteDeleted : MovieDetailNavigation()
        object MovieDetailError : MovieDetailNavigation()
    }

    fun onMovieDetailValidate() {
        addDisposable {
            isFavoriteMovieUseCase
                .invoke(movie)
                .subscribe({ result ->
                    if (result) {
                        setIsFavorite()
                    } else {
                        setIsNotFavorite()
                    }
                }, { setError() })
        }
    }

    fun onToggleFavoriteStatus() {
        if (isFavorite) {
            addDisposable {
                deleteFavoriteMovieUseCase
                    .invoke(movie)
                    .subscribe({ setIsNotFavorite() }, { setError() })
            }
        } else {
            addDisposable {
                createFavoriteMovieUserCase
                    .invoke(movie)
                    .subscribe({ setIsFavorite() }, { setError() })
            }
        }
    }

    private fun setIsFavorite() {
        isFavorite = true
        localEvents.value = Event(MovieDetailNavigation.FavoriteCreated)
    }

    private fun setIsNotFavorite() {
        isFavorite = false
        localEvents.value = Event(MovieDetailNavigation.FavoriteDeleted)
    }

    private fun setError() {
        isFavorite = true
        localEvents.value = Event(MovieDetailNavigation.MovieDetailError)
    }
}