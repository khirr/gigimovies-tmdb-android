package com.khirr.gigimovies.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.khirr.gigimovies.base.BaseViewModel
import com.khirr.gigimovies.base.Constants
import com.khirr.gigimovies.base.Event
import com.khirr.gigimovies.data.models.Movie
import com.khirr.gigimovies.usecases.SearchMoviesUseCase

class SearchMoviesListViewModel(
    private val searchMoviesUseCase: SearchMoviesUseCase
) : BaseViewModel() {
    // Events to emit
    private val localEvents = MutableLiveData<Event<SearchMoviesListNavigation>>()
    val events: LiveData<Event<SearchMoviesListNavigation>> get() = localEvents

    // Lifecycle vars
    private var currentPage = 1
    private var isLoading = false
    private var isLastPage = false
    private var query = ""

    sealed class SearchMoviesListNavigation {
        data class ShowSearchMovies(val movies: List<Movie>) : SearchMoviesListNavigation()
        data class ShowSearchMoviesError(val error: Throwable) : SearchMoviesListNavigation()
        object ClearExistingMovies : SearchMoviesListNavigation()
        object NoMoviesFound : SearchMoviesListNavigation()
        object ShowLoading : SearchMoviesListNavigation()
        object HideLoading : SearchMoviesListNavigation()
    }

    fun onSearchMovies(query: String) {
        this.query = query
        onClear()
        onSearchMovies()
    }

    fun onClear() {
        isLoading = false
        isLastPage = false
        currentPage = 1
        localEvents.value = Event(SearchMoviesListNavigation.ClearExistingMovies)
    }

    private fun onSearchMovies() {
        addDisposable {
            searchMoviesUseCase
                .invoke(query, currentPage)
                .doOnSubscribe { showIsLoading() }
                .subscribe({ movies ->
                    if (movies.size < Constants.MOVIES_API_DEFAULT_ITEMS_PER_PAGE) {
                        isLastPage = true
                    }

                    if (movies.isEmpty() && currentPage == 1) {
                        localEvents.value = Event(SearchMoviesListNavigation.NoMoviesFound)
                    } else {
                        localEvents.value = Event(SearchMoviesListNavigation.ShowSearchMovies(movies))
                    }

                    hideIsLoading()
                }, { error ->
                    isLastPage = true
                    hideIsLoading()
                    localEvents.value = Event(SearchMoviesListNavigation.ShowSearchMoviesError(error))
                })
        }
    }

    fun onLoadMore(visibleItemCount: Int, firstVisibleItemPosition: Int, totalItemCount: Int) {
        if (isLoading || isLastPage || !isInFooter(visibleItemCount, firstVisibleItemPosition, totalItemCount)) {
            return
        }

        currentPage += 1
        onSearchMovies()
    }

    private fun isInFooter(
        visibleItemCount: Int,
        firstVisibleItemPosition: Int,
        totalItemCount: Int
    ): Boolean {
        return visibleItemCount + firstVisibleItemPosition >= totalItemCount
                && firstVisibleItemPosition >= 0
                && totalItemCount >= Constants.MOVIES_API_DEFAULT_ITEMS_PER_PAGE
    }

    private fun showIsLoading() {
        isLoading = true
        localEvents.value = Event(SearchMoviesListNavigation.ShowLoading)
    }

    private fun hideIsLoading() {
        isLoading = false
        localEvents.value = Event(SearchMoviesListNavigation.HideLoading)
    }
}