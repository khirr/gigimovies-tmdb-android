package com.khirr.gigimovies.data.parsers

import com.khirr.gigimovies.base.Constants
import com.khirr.gigimovies.data.models.Movie
import com.khirr.gigimovies.data.room.entities.FavoriteMovieEntity

fun FavoriteMovieEntity.toModel(): Movie {
    return Movie(
        id = this.id,
        posterPath = this.posterPath,
        title = this.title,
        overview = this.overview
    )
}

fun Movie.toFavoriteMovieEntity(): FavoriteMovieEntity {
    return FavoriteMovieEntity(
        id = this.id,
        title = this.title,
        overview = this.overview,
        posterPath = this.posterPath
    )
}