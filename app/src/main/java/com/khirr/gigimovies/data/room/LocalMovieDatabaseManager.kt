package com.khirr.gigimovies.data.room

import android.app.Application
import androidx.room.Room
import com.khirr.gigimovies.base.Constants


class LocalMovieDatabaseManager(app: Application) {
    private val db = Room.databaseBuilder(
        app,
        LocalMovieDatabase::class.java, Constants.MOVIES_ROOM_DATABASE_NAME
    ).build()

    fun database(): LocalMovieDatabase = db
}