package com.khirr.gigimovies.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.khirr.gigimovies.data.room.entities.FavoriteMovieDao
import com.khirr.gigimovies.data.room.entities.FavoriteMovieEntity

@Database(entities = [FavoriteMovieEntity::class], version = 1)
abstract class LocalMovieDatabase : RoomDatabase() {
    abstract fun favoriteMovieDao(): FavoriteMovieDao
}