package com.khirr.gigimovies.data.retrofit

import com.khirr.gigimovies.base.Constants
import com.khirr.gigimovies.data.retrofit.response.MoviesServerResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface MovieService {
    @GET("movie/popular")
    fun findPopularMovies(
        @Query("page") page: Int = 1,
        @Query("api_key") apiKey: String = Constants.MOVIES_API_KEY,
    ): Single<MoviesServerResponse>

    @GET("search/movie")
    fun searchMovies(
        @Query("query", encoded = true) query: String,
        @Query("page") page: Int = 1,
        @Query("api_key") apiKey: String = Constants.MOVIES_API_KEY,
    ): Single<MoviesServerResponse>
}