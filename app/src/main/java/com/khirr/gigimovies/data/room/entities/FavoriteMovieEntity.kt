package com.khirr.gigimovies.data.room.entities

import androidx.room.*
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single

@Entity(
    tableName = "favorite_movies",
    indices = [Index(value = ["id"], unique = true)]
)
data class FavoriteMovieEntity(
    @PrimaryKey(autoGenerate = true) val uid: Int = 0,
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "overview") val overview: String,
    @ColumnInfo(name = "poster_path") val posterPath: String?,
)

@Dao
interface FavoriteMovieDao {
    @Query("SELECT * FROM favorite_movies ORDER BY uid DESC")
    fun findAll(): Flowable<List<FavoriteMovieEntity>>

    @Query("SELECT * FROM favorite_movies WHERE id = :id")
    fun findById(id: Int): Maybe<FavoriteMovieEntity>

    @Query("SELECT COUNT() FROM favorite_movies WHERE id = :id")
    fun count(id: Int): Single<Int>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg users: FavoriteMovieEntity): Completable

    @Delete
    fun delete(favoriteMovieEntity: FavoriteMovieEntity): Completable

    @Query("DELETE FROM favorite_movies WHERE id = :id")
    fun deleteById(id: Int): Completable
}