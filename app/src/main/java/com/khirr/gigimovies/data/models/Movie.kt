package com.khirr.gigimovies.data.models

import android.os.Parcelable
import com.khirr.gigimovies.base.Constants
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(
    val id: Int,
    val title: String,
    val overview: String,
    val posterPath: String?,
): Parcelable {
    fun posterOriginalUrl(): String? {
        if (posterPath == null) {
            return null
        }
        return Constants.MOVIES_IMAGES_ORIGINAL_BASE_URL + posterPath
    }
    fun posterThumbnailUrl(): String? {
        if (posterPath == null) {
            return null
        }
        return Constants.MOVIES_IMAGES_THUMBNAIL_BASE_URL + posterPath
    }
}