package com.khirr.gigimovies.data.retrofit.response

import com.google.gson.annotations.SerializedName

data class MovieServerResponse(
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("overview") val overview: String,
    @SerializedName("poster_path") val posterPath: String?,
)