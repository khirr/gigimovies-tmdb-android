package com.khirr.gigimovies.ui.movies.detail

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.khirr.gigimovies.R
import com.khirr.gigimovies.base.BaseActivity
import com.khirr.gigimovies.data.models.Movie
import com.khirr.gigimovies.databinding.ActivityMovieDetailBinding
import com.khirr.gigimovies.lib.bindImageUrl
import com.khirr.gigimovies.presentation.MovieDetailViewModel
import com.khirr.gigimovies.presentation.MovieDetailViewModel.MovieDetailNavigation
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class MovieDetailActivity : BaseActivity() {
    companion object {
        private const val PARAM_EXTRA_MOVIE = "movie"

        fun open(context: AppCompatActivity, movie: Movie) {
            val intent = Intent(context, MovieDetailActivity::class.java)
            intent.putExtra(PARAM_EXTRA_MOVIE, movie)
            context.startActivity(intent)
        }
    }

    private lateinit var binding: ActivityMovieDetailBinding
    private val movie: Movie by lazy {
        intent.extras!!.getParcelable(PARAM_EXTRA_MOVIE)!!
    }
    private val movieDetailViewModel: MovieDetailViewModel by inject { parametersOf(movie) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMovieDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        setBackArrow(binding.toolbar)
        title = movie.title

        binding.fab.setOnClickListener {
            movieDetailViewModel.onToggleFavoriteStatus()
        }

        binding.content.posterImageView.bindImageUrl(
            url = movie.posterOriginalUrl(),
            errorPlaceholder = R.drawable.outline_broken_image_24,
            placeholder = R.drawable.outline_image_24
        )

        binding.content.movie = movie

        movieDetailViewModel.events.observe(this, { event ->
            event.getContentIfNotHandled()?.let { navigation ->
                when (navigation) {
                    is MovieDetailNavigation.FavoriteCreated -> {
                        binding.fab.setImageResource(R.drawable.outline_favorite_24)
                    }
                    is MovieDetailNavigation.FavoriteDeleted -> {
                        binding.fab.setImageResource(R.drawable.outline_favorite_border_24)
                    }
                    is MovieDetailNavigation.MovieDetailError -> {
                        Toast.makeText(
                            this,
                            R.string.unexpected_error,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        })


        movieDetailViewModel.onMovieDetailValidate()
    }
}