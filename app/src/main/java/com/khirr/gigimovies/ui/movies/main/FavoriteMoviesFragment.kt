package com.khirr.gigimovies.ui.movies.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.khirr.gigimovies.R
import com.khirr.gigimovies.base.BaseFragment
import com.khirr.gigimovies.databinding.FragmentMoviesListBinding
import com.khirr.gigimovies.presentation.FavoriteMoviesListViewModel
import com.khirr.gigimovies.presentation.FavoriteMoviesListViewModel.FavoriteMoviesListNavigation
import kotlinx.android.synthetic.main.fragment_movies_list.*
import org.koin.android.ext.android.inject

class FavoriteMoviesFragment : BaseFragment(R.string.favorites) {
    private lateinit var binding: FragmentMoviesListBinding
    private val recyclerViewAdapter by lazy { MoviesListAdapter() }
    private val favoriteMoviesListViewModel: FavoriteMoviesListViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMoviesListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager = GridLayoutManager(context, 2)
        binding.recyclerView.adapter = recyclerViewAdapter

        // Listen updates on favorites movies and pass to onFavoriteMoviesList
        // To dispatch events
        favoriteMoviesListViewModel.favoriteMoviesList.observe(
            viewLifecycleOwner,
            favoriteMoviesListViewModel::onFavoriteMoviesList
        )

        // Set message for empty list
        binding.messageTextView.text = getString(R.string.you_do_not_have_favorites_yet)

        favoriteMoviesListViewModel.events.observe(viewLifecycleOwner, { event ->
            event.getContentIfNotHandled()?.let { navigation ->
                when (navigation) {
                    is FavoriteMoviesListNavigation.ShowFavoriteMovies -> {
                        binding.messageTextView.visibility = View.INVISIBLE
                        recyclerViewAdapter.setItems(navigation.movies)
                    }
                    FavoriteMoviesListNavigation.FavoriteMoviesEmpty -> {
                        binding.messageTextView.visibility = View.VISIBLE
                    }
                }
            }
        })

    }

}