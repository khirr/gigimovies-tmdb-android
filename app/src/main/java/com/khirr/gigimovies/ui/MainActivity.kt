package com.khirr.gigimovies.ui

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.khirr.gigimovies.R
import com.khirr.gigimovies.presentation.PopularMoviesListViewModel
import com.khirr.gigimovies.ui.movies.main.MoviesTabActivity
import com.khirr.gigimovies.ui.movies.search.SearchMoviesActivity
import org.koin.android.ext.android.inject


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MoviesTabActivity.open(this)
        finish()
    }
}