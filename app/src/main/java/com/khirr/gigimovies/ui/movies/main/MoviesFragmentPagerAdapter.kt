package com.khirr.gigimovies.ui.movies.main

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.khirr.gigimovies.base.BaseFragment

class MoviesFragmentPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT ) {
    private val fragments = ArrayList<BaseFragment>()

    fun addItem(fragment: BaseFragment) = fragments.add(fragment)

    override fun getCount(): Int  = fragments.size

    override fun getItem(position: Int): BaseFragment {
        return fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence {
        return fragments[position].title
    }
}