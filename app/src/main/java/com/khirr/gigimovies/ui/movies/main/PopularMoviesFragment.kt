package com.khirr.gigimovies.ui.movies.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.khirr.gigimovies.R
import com.khirr.gigimovies.base.BaseFragment
import com.khirr.gigimovies.databinding.FragmentMoviesListBinding
import com.khirr.gigimovies.presentation.PopularMoviesListViewModel
import com.khirr.gigimovies.presentation.PopularMoviesListViewModel.PopularMoviesListNavigation
import kotlinx.android.synthetic.main.fragment_movies_list.*
import org.koin.android.ext.android.inject

class PopularMoviesFragment : BaseFragment(R.string.popular) {

    private lateinit var binding: FragmentMoviesListBinding
    private val recyclerViewAdapter by lazy { MoviesListAdapter() }
    private val popularMoviesListViewModel: PopularMoviesListViewModel by inject()

    private val onScrollListener: RecyclerView.OnScrollListener by lazy {
        object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val layoutManager = recyclerView.layoutManager as GridLayoutManager
                val visibleItemCount: Int = layoutManager.childCount
                val totalItemCount: Int = layoutManager.itemCount
                val firstVisibleItemPosition: Int = layoutManager.findFirstVisibleItemPosition()

                popularMoviesListViewModel.onLoadMorePopularMovies(
                    visibleItemCount,
                    firstVisibleItemPosition,
                    totalItemCount
                )
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentMoviesListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager = GridLayoutManager(context, 2)
        binding.recyclerView.adapter = recyclerViewAdapter
        binding.recyclerView.addOnScrollListener(onScrollListener)

        popularMoviesListViewModel.events.observe(viewLifecycleOwner, Observer { event ->
            event.getContentIfNotHandled()?.let { navigation ->
                when (navigation) {
                    is PopularMoviesListNavigation.ShowLoading -> {
                        progressBar.visibility = View.VISIBLE
                    }
                    PopularMoviesListNavigation.HideLoading -> {
                        progressBar.visibility = View.INVISIBLE
                    }
                    is PopularMoviesListNavigation.PopularMoviesError -> {
                        Toast.makeText(
                            activity,
                            R.string.unexpected_error,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    is PopularMoviesListNavigation.ShowPopularMovies -> {
                        recyclerViewAdapter.addItems(navigation.movies)
                    }
                }
            }
        })

        popularMoviesListViewModel.onFindAllPopularMovies()
    }
}