package com.khirr.gigimovies.ui.movies.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.khirr.gigimovies.R
import com.khirr.gigimovies.data.models.Movie
import com.khirr.gigimovies.databinding.ItemMoviesListMovieBinding
import com.khirr.gigimovies.lib.bindImageUrl
import com.khirr.gigimovies.ui.movies.detail.MovieDetailActivity

class MoviesListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items = ArrayList<Movie>()

    fun addItems(newItems: List<Movie>) {
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun setItems(newItems: List<Movie>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMoviesListMovieBinding.inflate(inflater, parent, false)
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MovieViewHolder -> holder.bind(items[position])
        }
    }

    override fun getItemCount(): Int = items.size

    class MovieViewHolder(private val binding: ItemMoviesListMovieBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(movie: Movie) {
            binding.movie = movie
            binding.posterImageView.bindImageUrl(
                url = movie.posterThumbnailUrl(),
                errorPlaceholder = R.drawable.outline_broken_image_24,
                placeholder = R.drawable.outline_image_24
            )
            binding.containerCardView.setOnClickListener {
                MovieDetailActivity.open(binding.root.context as AppCompatActivity, movie)
            }
        }
    }
}