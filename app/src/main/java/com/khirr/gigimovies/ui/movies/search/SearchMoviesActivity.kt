package com.khirr.gigimovies.ui.movies.search

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.khirr.gigimovies.R
import com.khirr.gigimovies.base.BaseActivity
import com.khirr.gigimovies.data.models.Movie
import com.khirr.gigimovies.databinding.ActivityMovieDetailBinding
import com.khirr.gigimovies.databinding.ActivitySearchMoviesBinding
import com.khirr.gigimovies.presentation.SearchMoviesListViewModel
import com.khirr.gigimovies.ui.movies.main.MoviesListAdapter
import com.khirr.gigimovies.ui.movies.main.MoviesTabActivity
import kotlinx.android.synthetic.main.activity_search_movies.*
import org.koin.android.ext.android.inject

class SearchMoviesActivity : BaseActivity() {
    companion object {
        fun open(context: AppCompatActivity) {
            val intent = Intent(context, SearchMoviesActivity::class.java)
            context.startActivity(intent)
        }
    }

    private lateinit var binding: ActivitySearchMoviesBinding
    private val recyclerViewAdapter by lazy { MoviesListAdapter() }
    private val searchMoviesListViewModel: SearchMoviesListViewModel by inject()

    private val onScrollListener: RecyclerView.OnScrollListener by lazy {
        object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val layoutManager = recyclerView.layoutManager as GridLayoutManager
                val visibleItemCount: Int = layoutManager.childCount
                val totalItemCount: Int = layoutManager.itemCount
                val firstVisibleItemPosition: Int = layoutManager.findFirstVisibleItemPosition()

                searchMoviesListViewModel.onLoadMore(
                    visibleItemCount,
                    firstVisibleItemPosition,
                    totalItemCount
                )
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchMoviesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        setBackArrow(binding.toolbar)
        focusSearchView()

        binding.bodyView.recyclerView.layoutManager = GridLayoutManager(this, 2)
        binding.bodyView.recyclerView.adapter = recyclerViewAdapter
        binding.bodyView.recyclerView.addOnScrollListener(onScrollListener)

        binding.searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(body: String?): Boolean {
                if (!body.isNullOrBlank()) {
                    searchMoviesListViewModel.onSearchMovies(body)
                }
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                if (p0.isNullOrBlank()) {
                    searchMoviesListViewModel.onClear()
                    binding.bodyView.messageTextView.visibility = View.INVISIBLE
                }
                return false
            }
        })

        binding.bodyView.messageTextView.text = getString(R.string.not_results_found)

        searchMoviesListViewModel.events.observe(this, { event ->
            event.getContentIfNotHandled()?.let { navigation ->
                when (navigation) {
                    is SearchMoviesListViewModel.SearchMoviesListNavigation.ShowLoading -> {
                        binding.bodyView.progressBar.visibility = View.VISIBLE
                    }
                    is SearchMoviesListViewModel.SearchMoviesListNavigation.HideLoading -> {
                        binding.bodyView.progressBar.visibility = View.INVISIBLE
                    }
                    is SearchMoviesListViewModel.SearchMoviesListNavigation.NoMoviesFound -> {
                        binding.bodyView.messageTextView.visibility = View.VISIBLE
                    }
                    is SearchMoviesListViewModel.SearchMoviesListNavigation.ShowSearchMovies -> {
                        binding.bodyView.messageTextView.visibility = View.INVISIBLE
                        recyclerViewAdapter.addItems(navigation.movies)
                    }
                    is SearchMoviesListViewModel.SearchMoviesListNavigation.ShowSearchMoviesError -> {
                        Toast.makeText(
                            this,
                            R.string.unexpected_error,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    SearchMoviesListViewModel.SearchMoviesListNavigation.ClearExistingMovies -> {
                        recyclerViewAdapter.setItems(arrayListOf())
                    }
                }
            }
        })
    }

    private fun focusSearchView() {
        binding.searchView.setIconifiedByDefault(true)
        binding.searchView.isFocusable = true
        binding.searchView.isIconified = false
        binding.searchView.requestFocusFromTouch()
    }
}