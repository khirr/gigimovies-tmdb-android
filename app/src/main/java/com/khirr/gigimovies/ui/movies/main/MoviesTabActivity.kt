package com.khirr.gigimovies.ui.movies.main

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.khirr.gigimovies.R
import com.khirr.gigimovies.base.BaseActivity
import com.khirr.gigimovies.databinding.ActivityMoviesTabBinding
import com.khirr.gigimovies.ui.movies.search.SearchMoviesActivity
import kotlinx.android.synthetic.main.activity_movies_tab.*


class MoviesTabActivity : BaseActivity() {
    companion object {
        fun open(context: AppCompatActivity) {
            val intent = Intent(context, MoviesTabActivity::class.java)
            context.startActivity(intent)
        }
    }

    private lateinit var binding: ActivityMoviesTabBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMoviesTabBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(toolbar)
        title = getString(R.string.app_name)

        val viewPagerAdapter = MoviesFragmentPagerAdapter(supportFragmentManager)
        viewPagerAdapter.addItem(PopularMoviesFragment())
        viewPagerAdapter.addItem(FavoriteMoviesFragment())

        viewPager.adapter = viewPagerAdapter

        tabLayout.setupWithViewPager(viewPager);
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_movies_tab, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.open_search) {
            SearchMoviesActivity.open(this)
        }
        return super.onOptionsItemSelected(item)
    }
}